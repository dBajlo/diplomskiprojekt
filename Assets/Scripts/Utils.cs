using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils 
{
    public static Vector3 RandomPointOnPerimeter(Vector3 center, float radius)
    {
        float angle = Random.Range(0, 2 * Mathf.PI);
        Vector3 point = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * Mathf.Sqrt(radius);
        point += center;
        return point;
    }
}
