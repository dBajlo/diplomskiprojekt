using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour
{
    public float totalHitPoints;
    public float currentHitPoints;

    // Start is called before the first frame update
    void Start()
    {
        currentHitPoints = totalHitPoints;
    }

    private int GetParticleNumber()
    {
        return Mathf.Min((int)totalHitPoints, AsteroidConstants.MAX_PARTICLES_DROPPED);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //layer 7 is particle
        if(collision.gameObject.layer == 7)
        {
            currentHitPoints -= AsteroidConstants.PARTICLE_DAMAGE;
            if(currentHitPoints <= 0)
            {
                ParticleFactory.get().SpawnParticles(GetParticleNumber(), transform.position);
                Destroy(gameObject);
            }
        }
    }
}
