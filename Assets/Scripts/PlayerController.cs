using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    const float THRESHOLD_ROTATION_DISTANCE = 0.3f;
    const float MIN_TOUCH_DISTANCE = 1f;
    bool touching = false;
    private Vector3 lastPosition;
    // Start is called before the first frame update
    void Start()
    {
        lastPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(mouseRay, out hit)) {
            if(!Input.GetButton("Fire1"))
            {
                //touching ended
                touching = false;
            }
            else {
                float distance = Vector3.Distance(this.transform.position, hit.point);
                if (!touching)
                {
                    //first touch has to be within min distance to register
                    if (distance > MIN_TOUCH_DISTANCE)
                    {
                        return;
                    }
                    touching = true;
                }
               
                if(distance > 0.0f)
                {
                    float distanceFromLast = Vector3.Distance(hit.point, lastPosition);
                    if (distanceFromLast > THRESHOLD_ROTATION_DISTANCE)
                    {
                        this.transform.LookAt(new Vector3(hit.point.x, 0, hit.point.z));
                        lastPosition = hit.point;
                    }

                    this.transform.position = new Vector3(hit.point.x, 0, hit.point.z);
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == 8)
        {
            //game over
            Destroy(gameObject);
        }
    }
}
