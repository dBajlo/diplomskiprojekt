using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidFactory : MonoBehaviour
{
    public GameObject asteroidPrefab;

    public float spawnCooldown = 1f;
    public const float spawnRadius = 60f;
    public float forceMagnitude = 100000;
    float lastSpawn = 0;
    public float difficultyMultiplier;
    public const float MAX_DIFFICULTY = 2f;
    public const float MAX_DIFFICULTY_TIME = 60f;

    // Start is called before the first frame update
    void Start()
    {
        difficultyMultiplier = 0.1f;
    }

    // Update is called once per frame
    void Update()
    {
        float currentTime = Time.time;
        if(currentTime - lastSpawn >= spawnCooldown)
        {
            lastSpawn = currentTime;
            SpawnAsteroid();
        }
        if(difficultyMultiplier < MAX_DIFFICULTY)
        {
            difficultyMultiplier = (currentTime / MAX_DIFFICULTY_TIME) * MAX_DIFFICULTY;
        }
    }

    void SpawnAsteroid()
    {
        GameObject asteroid = Instantiate(asteroidPrefab, this.transform);
        AsteroidController asteroidController = asteroid.GetComponent<AsteroidController>();
        asteroidController.totalHitPoints = AsteroidConstants.ASTEROID_AVERAGE_HP * difficultyMultiplier;
        Vector3 asteroidPosition = Utils.RandomPointOnPerimeter(Vector3.zero, spawnRadius);
        asteroid.transform.position = asteroidPosition;
        Vector3 normalizedDirection = (Vector3.zero - asteroidPosition).normalized;
        asteroid.GetComponent<Rigidbody>().AddForce(normalizedDirection * forceMagnitude);
    }
}
