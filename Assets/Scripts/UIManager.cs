using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    Text particleText;
    Text fpsCounter;
    static UIManager ui;
    float timer = 0f;
    float cooldownTime = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        ui = this;
        particleText = GameObject.Find("ParticleNumberText").GetComponent<Text>();
        fpsCounter = GameObject.Find("FPSCounter").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= cooldownTime)
        {
            timer = 0;
            fpsCounter.text = "FPS: " + ((int)(1.0f / Time.smoothDeltaTime)).ToString();
        }
    }

    public void SetParticleNumber(int n)
    {
        particleText.text = "Particles: " + n.ToString();
    }

    public static UIManager get()
    {
        return ui;
    }
}
