using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    public Transform playerTransform;
    public Rigidbody rigidbody;
    public Rigidbody playerRigidbody;
    public float playerMass;
    bool gravityReversed = false;
    float rotationDirection = 1f;

    public float totalHitPoints;
    public float currentHitPoints;

    void Start()
    {
        GameObject player = GameObject.Find("Player");
        playerTransform = player.GetComponent<Transform>();
        playerRigidbody = player.GetComponent<Rigidbody>();
        playerMass = playerRigidbody.mass;
        rigidbody = GetComponent<Rigidbody>();
        currentHitPoints = totalHitPoints;
        float maxMass = ParticleConstants.PARTICLE_AVG_MASS + ParticleConstants.PARTICLE_MASS_VARIATION;
        float coef = rigidbody.mass / maxMass;
        transform.localScale *= coef;
    }

    float GetForceMagnitude(float sqrDistance)
    {
        float sign = 1f;
        if(!gravityReversed && sqrDistance < ParticleConstants.GRAVITY_REVERSE_THRESHOLD)
        {
            gravityReversed = true;
        }
        if (gravityReversed)
        {
            sign = -1f;
            if(sqrDistance > ParticleConstants.GRAVITY_RETURN_THRESHOLD)
            {
                gravityReversed = false;
            }
        }
        if(sqrDistance > ParticleConstants.GRAVITY_INCREASE_THRESHOLD)
        {
            //gravity ignores distance to return particle quicker to it's owner
            return ParticleConstants.G * playerMass * rigidbody.mass;
        }
        //Fg = G*m1*m2/r*r
        return sign * ParticleConstants.G * playerMass * rigidbody.mass / sqrDistance;
    }

    void Update()
    {
        Vector3 direction = playerTransform.position - transform.position;
        float sqrDistance = Vector3.SqrMagnitude(direction);
        if (sqrDistance > ParticleConstants.MIN_DISTANCE_FOR_GRAVITY)
        {
            Vector3 normalizedDirection = direction.normalized;
            float magnitude = GetForceMagnitude(sqrDistance);
            rigidbody.AddForce(normalizedDirection*magnitude);
            Vector3 tangent = Vector3.Cross(normalizedDirection, Vector3.up);
            rigidbody.AddForce(tangent*ParticleConstants.TANGENTIAL_MAGNITUDE*rotationDirection);
            rigidbody.velocity = new Vector3(rigidbody.velocity.x * ParticleConstants.DAMPING,
                                   rigidbody.velocity.y * ParticleConstants.DAMPING,
                                   rigidbody.velocity.z * ParticleConstants.DAMPING);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //layer 8 is asteroid
        if(collision.gameObject.layer == 8)
        {
            currentHitPoints -= ParticleConstants.PARTICLE_SELF_DMG;
            if(currentHitPoints <= 0)
            {
                Destroy(gameObject);
            }
            else
            {
                rotationDirection *= -1;
            }
        }
    }
}
