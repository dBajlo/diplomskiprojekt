using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleFactory : MonoBehaviour
{
    public int initalParticleNumber = 10;
    public float spawnRadius = 4;
    public GameObject particlePrefab;
    static ParticleFactory particleFactory;
    UIManager ui;
    

    public void SpawnParticles(int n, Vector3 position)
    {
        for(int i = 0; i < n; i++)
        {
            Vector3 particlePosition = Utils.RandomPointOnPerimeter(position, spawnRadius);
            GameObject newParticle = Instantiate(particlePrefab, this.transform);
            newParticle.transform.position = particlePosition;
            newParticle.GetComponent<Rigidbody>().mass = Random.Range(ParticleConstants.PARTICLE_AVG_MASS - ParticleConstants.PARTICLE_MASS_VARIATION,
                                                                      ParticleConstants.PARTICLE_AVG_MASS + ParticleConstants.PARTICLE_MASS_VARIATION);
            newParticle.GetComponent<ParticleController>().totalHitPoints = ParticleConstants.PARTICLE_AVG_HP;
        }
    }

    public static ParticleFactory get()
    {
        return particleFactory;
    }

    

    // Start is called before the first frame update
    void Start()
    {
        particleFactory = this;
        SpawnParticles(initalParticleNumber, Vector3.zero);
    }

    // Update is called once per frame
    void Update()
    {
        if (!ui)
        {
            ui = UIManager.get();
        }
        ui.SetParticleNumber(transform.childCount);
    }
}
