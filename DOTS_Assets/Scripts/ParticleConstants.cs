
public class ParticleConstants
{
    public const float G = 2f;
    public const float GRAVITY_REVERSE_THRESHOLD = 2f;
    public const float GRAVITY_RETURN_THRESHOLD = 3f;
    public const float GRAVITY_INCREASE_THRESHOLD = 15f;
    public const float MIN_DISTANCE_FOR_GRAVITY = 0.1f;
    public const float DAMPING = 0.99f;
    public const float TANGENTIAL_MAGNITUDE = 0.5f;
    public const float PARTICLE_AVG_HP = 5f;
    public const float PARTICLE_SELF_DMG = 1f;
    public const float PARTICLE_AVG_MASS = 1.5f;
    public const float PARTICLE_MASS_VARIATION = 0.5f;
    public const float FRICTION_COEFICIENT = 0.04f;
}
