using Unity.Entities;
using UnityEngine;
using Unity.Transforms;
using Unity.Mathematics;
using System.Collections;
using System.Collections.Generic;
using Unity.Physics;

public class ParticleFactory : MonoBehaviour
{
    public struct ParticleNumberAndPosition
    {
        public int number;
        public float3 position;
    }

    public GameObject particlePrefab;
    public static Entity particleEntityPrefab;
    private World defaultWorld;
    private BlobAssetStore blobAssetStore;
    private EntityManager entityManager;
    public int startingParticleNumber = 10;
    private static ParticleFactory instance;
    public const float maxMass = ParticleConstants.PARTICLE_AVG_MASS + ParticleConstants.PARTICLE_MASS_VARIATION;
    public static float spawnRadius = 2f;
    public static List<ParticleNumberAndPosition> scheduledParticles = new List<ParticleNumberAndPosition>();

    // Start is called before the first frame update
    void Start()
    {
        defaultWorld = World.DefaultGameObjectInjectionWorld;
        entityManager = defaultWorld.EntityManager;
        //ima podatke o tome kako konvertirati
        blobAssetStore = new BlobAssetStore();
        GameObjectConversionSettings settings = GameObjectConversionSettings.FromWorld(defaultWorld, blobAssetStore);
        //pretvara gameobject i njegovu djecu u entity
        particleEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(particlePrefab, settings);
        scheduledParticles.Add(new ParticleNumberAndPosition { number = startingParticleNumber, position = float3.zero });
        instance = this;
    }

    void Update() {
        foreach(ParticleNumberAndPosition p in scheduledParticles)
        {
            //Debug.Log("Stvaram " + p.number + " particlea na poziciji " + p.position);
            SpawnParticles(p.number, p.position, spawnRadius);
        }
        scheduledParticles.Clear();
    }

    private void OnDestroy()
    {
        blobAssetStore.Dispose();
    }


    public static ParticleFactory get()
    {
        return instance;
    }

    public void SpawnParticles(int n, Vector3 position, float spawnRadius)
    {
        for (int i = 0; i < n; i++)
        {
            float3 particlePosition = (float3)Utils.RandomPointOnPerimeter(position, spawnRadius);
            Entity newParticle = entityManager.Instantiate(particleEntityPrefab);

            //POSITION
            entityManager.SetComponentData(newParticle, new Translation { Value = particlePosition });

            //MASS
            float mass = GetParticleMass();
            PhysicsMass physicsMass = entityManager.GetComponentData<PhysicsMass>(newParticle);
            physicsMass.InverseMass = 1 / mass;
            entityManager.SetComponentData(newParticle, physicsMass);

            //SCALE
            float coef = GetMassCoeficient(mass);

            CompositeScale scale = entityManager.GetComponentData<CompositeScale>(newParticle);
            scale.Value[3][3] = 1 / coef;
            entityManager.SetComponentData(newParticle, scale);

            //PARTICLE DATA
            entityManager.SetComponentData(newParticle, GetParticleData()); 
        }
    }

    public static float GetParticleMass()
    {
        return Utils.random.NextFloat(ParticleConstants.PARTICLE_AVG_MASS - ParticleConstants.PARTICLE_MASS_VARIATION,
                                                                      ParticleConstants.PARTICLE_AVG_MASS + ParticleConstants.PARTICLE_MASS_VARIATION);
    }

    public static float GetMassCoeficient(float mass)
    {
        return mass / maxMass;
    }


    public static ParticleData GetParticleData()
    {
        return new ParticleData
        {
            gravityReversed = false,
            rotationDirection = 1f,
            totalHitPoints = ParticleConstants.PARTICLE_AVG_HP,
            currentHitPoints = ParticleConstants.PARTICLE_AVG_HP
        };
    }
}
