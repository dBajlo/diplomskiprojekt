using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[Serializable]
[GenerateAuthoringComponent]
public struct AsteroidData : IComponentData
{
    public float totalHitPoints;
    public float currentHitPoints;
    public float3 moveForceVector;
}
