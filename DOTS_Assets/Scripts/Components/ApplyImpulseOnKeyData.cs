using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[Serializable]
[GenerateAuthoringComponent]
public struct ApplyImpulseOnKeyData : IComponentData
{
    public float Force;
    public float3 Direction;
}
