using Unity.Entities;


[GenerateAuthoringComponent]
public struct ParticleData : IComponentData
{
    public bool gravityReversed;
    public float rotationDirection;
    public float totalHitPoints;
    public float currentHitPoints;
}

