using Unity.Entities;
using UnityEngine;
using Unity.Transforms;
using Unity.Mathematics;

public class AsteroidFactory : MonoBehaviour
{
    public GameObject asteroidPrefab;
    private Entity asteroidEntityPrefab;
    private World defaultWorld;
    private EntityManager entityManager;
    public static AsteroidFactory instance;
    private BlobAssetStore blobAssetStore;

    public float spawnCooldown = 1f;
    public const float spawnRadius = 60f;
    public float forceMagnitude;
    float lastSpawn = 0;
    public float difficultyMultiplier;
    public const float MAX_DIFFICULTY = 2f;
    public const float MAX_DIFFICULTY_TIME = 60f;

    public bool isEnabled = true;

    void Start()
    {
        instance = this;
        defaultWorld = World.DefaultGameObjectInjectionWorld;
        entityManager = defaultWorld.EntityManager;
        //ima podatke o tome kako konvertirati
        blobAssetStore = new BlobAssetStore();
        GameObjectConversionSettings settings = GameObjectConversionSettings.FromWorld(defaultWorld, blobAssetStore);
        //pretvara gameobject i njegovu djecu u entity
        asteroidEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(asteroidPrefab, settings);
        

        difficultyMultiplier = 0.1f;
        forceMagnitude = 1;
    }

    public static AsteroidFactory get()
    {
        return instance;
    }


    void Update()
    {
        if (!isEnabled) return;
        float currentTime = Time.time;
        if (currentTime - lastSpawn >= spawnCooldown)
        {
            lastSpawn = currentTime;
            SpawnAsteroid();
        }
        if (difficultyMultiplier < MAX_DIFFICULTY)
        {
            difficultyMultiplier = (currentTime / MAX_DIFFICULTY_TIME) * MAX_DIFFICULTY;
        }
    }

    private void OnDestroy()
    {
        blobAssetStore.Dispose();
    }

    public void SpawnAsteroid()
    {
        Entity newAsteroid = entityManager.Instantiate(asteroidEntityPrefab);

        //POSITION
        Vector3 asteroidPosition = Utils.RandomPointOnPerimeter(Vector3.zero, spawnRadius);
        entityManager.SetComponentData(newAsteroid, new Translation { Value = asteroidPosition });

        //HITPOINTS & FORCE DIRECTION
        float3 moveForce = (Vector3.zero - asteroidPosition).normalized * forceMagnitude;
        float totalHitPoints = AsteroidConstants.ASTEROID_AVERAGE_HP * difficultyMultiplier;
        entityManager.SetComponentData(newAsteroid, new AsteroidData { totalHitPoints = totalHitPoints, 
            currentHitPoints = totalHitPoints, moveForceVector = new float3(moveForce) });
    }
}
