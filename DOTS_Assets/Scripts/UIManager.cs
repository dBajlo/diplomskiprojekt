using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    Text particleText;
    Text fpsCounter;
    float timer = 0f;
    float cooldownTime = 0.2f;
    public static int particleNumber = 0;

    // Start is called before the first frame update
    void Start()
    {
        particleText = GameObject.Find("ParticleNumberText").GetComponent<Text>();
        fpsCounter = GameObject.Find("FPSCounter").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= cooldownTime)
        {
            timer = 0;
            fpsCounter.text = "FPS: " + ((int)(1.0f / Time.smoothDeltaTime)).ToString();
            particleText.text = "Particles: " + particleNumber.ToString();
        }
    }
}
