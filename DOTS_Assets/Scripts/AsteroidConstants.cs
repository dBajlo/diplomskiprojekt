using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidConstants 
{
    public const float PARTICLE_DAMAGE = 1f;
    public const float ASTEROID_AVERAGE_HP = 50f;
    public const int MAX_PARTICLES_DROPPED = 8;
    public const float PARTICLE_SPAWN_RADIUS = 1f;
}
