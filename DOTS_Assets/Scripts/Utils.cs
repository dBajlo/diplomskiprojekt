using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
public class Utils 
{
    public static Unity.Mathematics.Random random = new Unity.Mathematics.Random(1851936439);
    public static Vector3 RandomPointOnPerimeter(Vector3 center, float radius)
    {
        float angle = random.NextFloat(0, 2 * Mathf.PI);
        Vector3 point = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * Mathf.Sqrt(radius);
        point += center;
        return point;
    }
    public static float Float3SquareDistance(float3 a, float3 b)
    {
        return Mathf.Pow(a.x - b.x, 2) + Mathf.Pow(a.y - b.y, 2) + Mathf.Pow(a.z - b.z, 2);
    }

    public static float Float3Distance(float3 a, float3 b)
    {
        return Mathf.Sqrt(Float3SquareDistance(a, b));
    }

    public static float3 Float3CrossProduct(float3 a, float3 b)
    {
        float x = a.y * b.z - a.z * b.y;
        float y = -(a.x * b.z - a.z * b.x);
        float z = a.x * b.y - a.y * b.x;
        return new float3(x, y, z);
    }
}
