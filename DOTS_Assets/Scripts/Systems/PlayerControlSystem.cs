using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class PlayerControlSystem : SystemBase
{
    bool touching = false;
    const float THRESHOLD_ROTATION_DISTANCE = 0.3f;
    const float MIN_TOUCH_DISTANCE = 1f;
    private float3 lastPosition = float3.zero;
    protected override void OnUpdate()
    {
        Entities.WithoutBurst().WithAll<PlayerTag>()
                .ForEach((ref Translation translation) =>
                {
                    float3 mouseWoldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    mouseWoldPosition.y = 0;

                    if (!Input.GetButton("Fire1"))
                    {
                        //touching ended
                        touching = false;
                    }
                    else
                    {
                        float distance = Utils.Float3Distance(translation.Value, mouseWoldPosition);

                        if (!touching)
                        {
                            //first touch has to be within min distance to register
                            if (distance > MIN_TOUCH_DISTANCE)
                            {
                                return;
                            }
                            touching = true;
                        }

                        if (distance > 0.0f)
                        {
                            float distanceFromLast = Vector3.Distance(mouseWoldPosition, lastPosition);
                            if (distanceFromLast > THRESHOLD_ROTATION_DISTANCE)
                            {
                                /*
                                this.transform.LookAt(new Vector3(hit.point.x, 0, hit.point.z));
                                lastPosition = hit.point;*/
                            }
                            translation.Value = new float3(mouseWoldPosition);
                        }
                    }
                }).Run();
    }


}
