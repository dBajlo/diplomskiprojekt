using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;
using UnityEngine;
using Unity.Physics.Extensions;

public class ParticleAttractionSystem : SystemBase
{
    Entity playerEntity;
    float playerMass;
    float3 vectorUp;
    protected override void OnStartRunning()
    {
        playerEntity = GetSingletonEntity<PlayerTag>();
        playerMass = 1 / EntityManager.GetComponentData<PhysicsMass>(playerEntity).InverseMass;
        vectorUp = new float3(0, 1, 0);
    }
    protected override void OnUpdate()
    {

        Entities.WithoutBurst()
            .ForEach(
        (
            ref PhysicsVelocity physicsVelocity,
            ref PhysicsMass physicsMass,
            ref ParticleData particleData,
            in Translation translation) =>
        {
            float3 velocityVector = physicsVelocity.GetLinearVelocity(physicsMass, new Translation { Value = float3.zero },
                new Rotation { Value = quaternion.identity }, float3.zero);
            float velocitySquared = Utils.Float3SquareDistance(velocityVector, float3.zero);
            float velocity = Mathf.Sqrt(velocitySquared);

            //APPLY FRICTION
            float3 frictionVector = GetFrictionVector(velocityVector, velocity);
            physicsVelocity.ApplyLinearImpulse(physicsMass, frictionVector);

            //APPLY ATTRACTION
            Translation playerTranslation = EntityManager.GetComponentData<Translation>(playerEntity);
            float3 direction = playerTranslation.Value - translation.Value;
            float sqrDistance = Utils.Float3SquareDistance(playerTranslation.Value, translation.Value);
            float3 distance = Mathf.Sqrt(sqrDistance);
            float3 normalizedDirection = direction / distance;
            float magnitude = GetForceMagnitude(sqrDistance, particleData, 1 / physicsMass.InverseMass);
            physicsVelocity.ApplyLinearImpulse(physicsMass, normalizedDirection * magnitude);

            //APPLY TANGENTIAL FORCE
            float3 tangent = Utils.Float3CrossProduct(normalizedDirection, vectorUp);
            physicsVelocity.ApplyLinearImpulse(physicsMass, tangent * particleData.rotationDirection * ParticleConstants.TANGENTIAL_MAGNITUDE);

        }).Run();
    }

    float3 GetFrictionVector(float3 velocityVector, float velocity)
    {
        if (velocity < ParticleConstants.MIN_DISTANCE_FOR_GRAVITY) return float3.zero;
        float3 normalizedDirection = -velocityVector / velocity;
        float3 frictionMagnitude = velocity * ParticleConstants.FRICTION_COEFICIENT;
        return new float3(normalizedDirection * frictionMagnitude);
    }

    float GetForceMagnitude(float sqrDistance, ParticleData particleData, float particleMass)
    {
        float sign = 1f;
        if (!particleData.gravityReversed && sqrDistance < ParticleConstants.GRAVITY_REVERSE_THRESHOLD)
        {
            particleData.gravityReversed = true;
        }
        if (particleData.gravityReversed)
        {
            sign = -1f;
            if (sqrDistance > ParticleConstants.GRAVITY_RETURN_THRESHOLD)
            {
                particleData.gravityReversed = false;
            }
        }
        if (sqrDistance > ParticleConstants.GRAVITY_INCREASE_THRESHOLD)
        {
            //gravity ignores distance to return particle quicker to it's owner
            return ParticleConstants.G * playerMass * particleMass;
        }
        //Fg = G*m1*m2/r*r
        sqrDistance = Mathf.Max(sqrDistance, ParticleConstants.MIN_DISTANCE_FOR_GRAVITY);
        return sign * ParticleConstants.G * playerMass * particleMass / sqrDistance;
    }
}
