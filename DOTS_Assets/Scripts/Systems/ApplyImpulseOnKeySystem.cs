using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;
using UnityEngine;
using Unity.Physics.Extensions;


public class ApplyImpulseOnKeySystem : SystemBase
{

    protected override void OnUpdate()
    {

        Entities.WithoutBurst().ForEach(
        (
            ref PhysicsVelocity _physicsVelocity,
            ref PhysicsMass _physicsMass,
            in ApplyImpulseOnKeyData _applyImpulseOnKeyData) =>
        {
            float3 impulse = _applyImpulseOnKeyData.Direction * _applyImpulseOnKeyData.Force;
            /// Apply a linear impulse to the entity.
            //Debug.Log("direction " + _applyImpulseOnKeyData.Direction.ToString());
            //PhysicsComponentExtensions.ApplyLinearImpulse(ref _physicsVelocity, _physicsMass, impulse);
            _physicsVelocity.ApplyLinearImpulse(_physicsMass, impulse);

        }).Run();
    }
}
