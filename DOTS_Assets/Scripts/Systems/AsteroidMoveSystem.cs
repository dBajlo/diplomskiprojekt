using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using Unity.Physics.Extensions;

public class AsteroidMoveSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Entities.ForEach((ref PhysicsVelocity physicsVelocity, ref PhysicsMass physicsMass, in AsteroidData data) => {
            physicsVelocity.ApplyLinearImpulse(physicsMass, data.moveForceVector);
        }).Schedule();
    }
}
