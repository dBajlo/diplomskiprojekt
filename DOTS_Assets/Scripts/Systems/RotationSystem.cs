using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class NewSystem : SystemBase
{
    protected override void OnUpdate()
    {

        Entities.ForEach((ref Rotation rotation, in RotationSpeed rotationSpeed) => {
            rotation.Value = math.mul(rotation.Value, quaternion.RotateY(rotationSpeed.Value));
            //     translation.Value += math.mul(rotation.Value, new float3(0, 0, 1)) * deltaTime;
        }).Schedule();
    }
}
