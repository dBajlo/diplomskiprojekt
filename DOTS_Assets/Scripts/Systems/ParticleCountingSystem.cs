using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class ParticleCountingSystem : SystemBase
{
    protected override void OnUpdate()
    {
        EntityQuery query = GetEntityQuery(ComponentType.ReadOnly<ParticleData>());
        UIManager.particleNumber = query.CalculateEntityCount();
    }
}
