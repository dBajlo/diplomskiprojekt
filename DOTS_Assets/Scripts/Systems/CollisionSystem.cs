using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

public class CollisionSystem : JobComponentSystem
{
    public Entity particlePrefab;

    private struct CollisionJob : ICollisionEventsJob
    {
        public ComponentDataFromEntity<AsteroidData> AsteroidDataGroup;
        public ComponentDataFromEntity<ParticleData> ParticleDataGroup;
        public ComponentDataFromEntity<PlayerTag> PlayerTagGroup;
        public ComponentDataFromEntity<Translation> TranslationGroup;
        public ComponentDataFromEntity<CompositeScale> CompositeScaleGroup;
        public ComponentDataFromEntity<PhysicsMass> PhysicsMassGroup;
        public EntityCommandBuffer CommandBuffer;
        public Entity particlePrefab;
        public const float maxMass = ParticleConstants.PARTICLE_AVG_MASS + ParticleConstants.PARTICLE_MASS_VARIATION;
        public void Execute(CollisionEvent collisionEvent)
        {
            //Debug.Log($"Collision between entities { collisionEvent.EntityA.Index } and { collisionEvent.EntityB.Index }");
            Entity entityA = collisionEvent.EntityA;
            Entity entityB = collisionEvent.EntityB;

            bool isAsteroidA = false, isAsteroidB = false;
            bool isParticleA = false, isParticleB = false;

            bool isPlayerA = PlayerTagGroup.HasComponent(entityA);
            bool isPlayerB = PlayerTagGroup.HasComponent(entityB);

            if (!isPlayerA) isAsteroidA = AsteroidDataGroup.HasComponent(entityA);
            if (!isPlayerB) isAsteroidB = AsteroidDataGroup.HasComponent(entityB);

            if (!isPlayerA && !isAsteroidA) isParticleA = ParticleDataGroup.HasComponent(entityA);
            if (!isPlayerB && !isAsteroidB) isParticleB = ParticleDataGroup.HasComponent(entityB);

            /*Possible collisions:
             * 2 asteroids: do nothing
             * asteroid and player: kill player
             * particle and asteroid: deal damage and destroy entities if hitpoints 0*/

            if (isAsteroidA && isPlayerB || isAsteroidB && isPlayerA)
            {
                Entity asteroidEntity = isAsteroidA ? entityA : entityB;
                Entity playerEntity = isPlayerA ? entityA : entityB;
                //Debug.Log("Collision between asteroid and player");
            }

            else if (isAsteroidA && isParticleB || isAsteroidB && isParticleA)
            {
                Entity asteroidEntity = isAsteroidA ? entityA : entityB;
                Entity particleEntity = isParticleA ? entityA : entityB;
                //Debug.Log("Collision between asteroid and particle");

                AsteroidData asteroidData = AsteroidDataGroup[asteroidEntity];
                ParticleData particleData = ParticleDataGroup[particleEntity];

                //ASJUST ASTEROID DATA
                asteroidData.currentHitPoints -= AsteroidConstants.PARTICLE_DAMAGE;
                if (asteroidData.currentHitPoints <= 0)
                {
                    Translation asteroidPosition = TranslationGroup[asteroidEntity];
                    int particleNumber = Mathf.Min((int)asteroidData.totalHitPoints, AsteroidConstants.MAX_PARTICLES_DROPPED);
                    //Debug.Log("Scheduling " + particleNumber + " particles on location " + asteroidPosition.Value);
                    ParticleFactory.scheduledParticles.Add(new ParticleFactory.ParticleNumberAndPosition { number = particleNumber, 
                        position = asteroidPosition.Value });
                    CommandBuffer.DestroyEntity(asteroidEntity);
                }
                else
                {
                    AsteroidDataGroup[asteroidEntity] = asteroidData;
                }

                //ADJUST PARTICLE DATA
                particleData.currentHitPoints -= ParticleConstants.PARTICLE_SELF_DMG;
                if (particleData.currentHitPoints <= 0)
                {
                    CommandBuffer.DestroyEntity(particleEntity);
                }
                else
                {
                    particleData.rotationDirection *= -1;
                    ParticleDataGroup[particleEntity] = particleData;
                }
            }

            else if(isAsteroidA && isAsteroidB)
            {
                //Debug.Log("Collision between 2 asteroids");
            }
        }
        
    }

    private BuildPhysicsWorld buildPhysicsWorldSystem;
    private StepPhysicsWorld stepPhysicsWorldSystem;
    private EndSimulationEntityCommandBufferSystem commandBufferSystem;

    protected override void OnCreate()
    {
        base.OnCreate();
        buildPhysicsWorldSystem = World.GetExistingSystem<BuildPhysicsWorld>();
        stepPhysicsWorldSystem = World.GetExistingSystem<StepPhysicsWorld>();
        commandBufferSystem = World.GetExistingSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnStartRunning()
    {
        base.OnStartRunning();
        particlePrefab = ParticleFactory.particleEntityPrefab;
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        JobHandle jobHandle = new CollisionJob {
            AsteroidDataGroup = GetComponentDataFromEntity<AsteroidData>(),
            ParticleDataGroup = GetComponentDataFromEntity<ParticleData>(),
            PlayerTagGroup = GetComponentDataFromEntity<PlayerTag>(),
            TranslationGroup = GetComponentDataFromEntity<Translation>(),
            CompositeScaleGroup = GetComponentDataFromEntity<CompositeScale>(),
            PhysicsMassGroup = GetComponentDataFromEntity<PhysicsMass>(),
            CommandBuffer = commandBufferSystem.CreateCommandBuffer(),
            particlePrefab = particlePrefab
        }.Schedule(
            stepPhysicsWorldSystem.Simulation,
            ref buildPhysicsWorldSystem.PhysicsWorld,
            inputDeps);
        commandBufferSystem.AddJobHandleForProducer(jobHandle);
        return jobHandle;
    }
}
