using Unity.Entities;
using UnityEngine;
using Unity.Transforms;
using Unity.Mathematics;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject gameObjectPrefab;
    private Entity entityPrefab;
    private World defaultWorld;
    private EntityManager entityManager;
    public bool enabled;
    // Start is called before the first frame update
    void Start()
    {
        if (!enabled) return;
        defaultWorld = World.DefaultGameObjectInjectionWorld;
        entityManager = defaultWorld.EntityManager;
        //ima podatke o tome kako konvertirati
        var blobAssetStore = new BlobAssetStore();
        GameObjectConversionSettings settings = GameObjectConversionSettings.FromWorld(defaultWorld, blobAssetStore);
        //pretvara gameobject i njegovu djecu u entity
        entityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(gameObjectPrefab, settings);
        InstantiateGrid(10, 10, 1f);
    }

    private void InstantiateGrid(int h, int w, float spacing = 1.0f)
    {
        for(int i = 0; i < h; i++)
        {
            for (int j = 0; j < w; j++)
            {
                InstantiateEntity(new float3(i*spacing, j*spacing, 0f));
            }
        }
    }

    private void InstantiateEntity(float3 position)
    {
        //stvori na 0,0,0
        Entity myEntity = entityManager.Instantiate(entityPrefab);
        //za pomicanje treba entity manager
        entityManager.SetComponentData(myEntity, new Translation
        {
            Value = position
        });
    }
}
